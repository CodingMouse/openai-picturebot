import openai
import os

from telegram import __version__ as TG_VER

try:
    from telegram import __version_info__

except ImportError:
    __version_info__ = (0, 0, 0, 0, 0)  # type: ignore[assignment]


if __version_info__ < (20, 0, 0, "alpha", 5):
    raise RuntimeError(
        f"This example is not compatible with your current PTB version {TG_VER}. To view the "
        f"{TG_VER} version of this example, "
        f"visit https://docs.python-telegram-bot.org/en/v{TG_VER}/examples.html"
    )

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update

from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)


OPEN_AI_KEY = os.getenv("OPENAI_API_KEY")
BOT_TOKEN = os.getenv("TEST_BOT_TOKEN")
PROMPT, SIZE, IDLE = range(3)
messagePrompts = dict()
sizePrompts = dict()



async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.message.reply_text(
        "Hi! I will create a picture based on the prompt that you give me.\n\n"
        "Send /cancel to stop talking to me.\n"
        "Send /restart to restart the process.\n\n"
        "What do you want me to generate? (example: a white cat)",
    )
    return PROMPT



async def prompt(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    # update user's prompt:
    messagePrompts['{}'.format(update.message.from_user.id)] = update.message.text\
    
    user = update.message.from_user

    reply_keyboard = [['256x256', '512x512', '1024x1024']]

    await update.message.reply_text(
        "Thank you! Now please tell me the size of the picture:\n",
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard, one_time_keyboard=True, input_field_placeholder="Select the size"
        ),
    )
    return SIZE


async def size(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    # update user's size selection:
    sizePrompts['{}'.format(update.message.from_user.id)] = update.message.text

    user = update.message.from_user

    await update.message.reply_text("Generating image, please wait...")

    response = openai.Image.create(prompt=messagePrompts['{}'.format(user.id)], n=1, size=sizePrompts['{}'.format(user.id)])

    image_url = response["data"][0]["url"]
    
    await update.message.reply_text("Thank you! Here's the link to your picture!\n{}\n".format(image_url))
    await update.message.reply_text("Send /cancel to stop talking to me.\nSend /restart to restart the process.")
    
    return IDLE



async def cancel(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    user = update.message.from_user
    await update.message.reply_text(
        "Bye! I hope we can talk again some day.", reply_markup=ReplyKeyboardRemove()
    )
    return ConversationHandler.END



async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Sorry, I didn't understand that command.")



def main() -> None:
    openai.api_key = OPEN_AI_KEY
    application = Application.builder().token(BOT_TOKEN).build()

    conv_handler = ConversationHandler(

        entry_points=[CommandHandler("start", start), CommandHandler("restart", start)],

        states={
            PROMPT: [MessageHandler(filters.TEXT & ~filters.COMMAND, prompt), CommandHandler("restart", start)],
            SIZE: [MessageHandler(filters.TEXT & ~filters.COMMAND, size), CommandHandler("restart", start)],
            IDLE: [CommandHandler("cancel", cancel), CommandHandler("start", start), CommandHandler("restart", start)],
        },

        fallbacks=[CommandHandler("cancel", cancel), MessageHandler(filters.COMMAND, unknown)],
    )

    application.add_handler(conv_handler)
    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)



if __name__ == "__main__":
    main()